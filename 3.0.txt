#include "stdio.h"
#include "stdlib.h"
#include "string.h"

void CharCount(char path[]);
void WordCount(char path[]);
void sentenceCount(char path[]);
void Muiltiple(char path[]);

int main()
{
	char input[100],path[50];
	printf("程序启动中\n\n");
    printf("请输入命令: wc.exe -options filePath\n\n");
    printf("选项:-c/-w/-s/-a\n\n");
    printf("请输入命令:");
    gets(input);
    int count=strlen(input);
    strncpy(path, input + 10,  count - 10);
    path[count - 9] = '\0';
    int check = 1;
    while (check)
    {
        int i = 7;
        if ((input[i] == '-') && (input[i + 1] == 'c'))
        {
            CharCount(path);
            break;
        }
        if ((input[i] == '-') && (input[i + 1] == 'w'))
        {
            WordCount(path);
            break;
        }
        if ((input[i] == '-') && (input[i + 1] == 's'))
        {
            sentenceCount(path);
            break;
        }
        if ((input[i] == '-') && (input[i + 1] == 'a'))
        {
            Muiltiple(path);
            break;
        }
    }
    system("pause");
    return 0;
}

void CharCount(char path[])
{
    FILE *fp=NULL;
    int c = 0;
    char ch;
    char *path3 = path;
    int k = strlen(path);
    *(path3 + k-1) = '\0';
    if ((fp = fopen(path3 , "r")) == NULL)
    {
        printf("读取失败\n\n");
        exit(0);
    }
    ch = fgetc(fp);
    while (ch != EOF)
    {
        c++;
        ch = fgetc(fp);
    }
    printf("字符数为（含标点与空格）：%d\n\n", c);
    fclose(fp);
}

void WordCount(char path[])
{
    FILE *fp;
    int w = 0;
    char ch;
    char *path3 = path;
    int k = strlen(path);
    *(path3 + k - 1) = '\0';
    if ((fp = fopen(path3, "r")) == NULL)
    {
        printf("读取失败\n\n");
        exit(0);
    }
    ch = fgetc(fp);
    while (ch != EOF)
    {
        if ((ch >= 'a'&&ch <= 'z') || (ch >= 'A'&&ch <= 'Z') || (ch >= '0'&&ch <= '9'))
        {
            while ((ch >= 'a'&&ch <= 'z') || (ch >= 'A'&&ch <= 'Z') || (ch >= '0'&&ch <= '9') || ch == '_')
            {
                ch = fgetc(fp);
            }
            w++;
            ch = fgetc(fp);
        }
        else
        {
            ch = fgetc(fp);
        }
    }
    printf("单词数为：%d\n\n", w);
    fclose(fp);
}

void sentenceCount(char path[])
{
    FILE *fp;
    int s = 0;
    char ch;
    char *path3 = path;
    int k = strlen(path);
    *(path3 + k - 1) = '\0';
    if ((fp = fopen(path3, "r")) == NULL)
    {
        printf("读取失败\n\n");
        exit(0);
    }
    ch = fgetc(fp);
    while (ch != EOF)
    {
        if (ch == '.')
        {
            s++;
            ch = fgetc(fp);
        }
        else
        {
            ch = fgetc(fp);
        }
    }
    printf("句子数为：%d\n\n", s);
    fclose(fp);
}

void Muiltiple(char path[])
{
    FILE *fp;
    char ch;
    char *path3 = path;
    int k = strlen(path);
    *(path3 + k - 1) = '\0';
    int c = 0, e = 0, n = 0;
    if ((fp = fopen(path3, "r")) == NULL)
    {
        printf("读取失败\n\n");
        exit(0);
    }

    ch = fgetc(fp);
    while (ch != EOF)
    {
        if (ch == '{' || ch == '}')
        {
            e++;
            ch = fgetc(fp);
        }
        else if (ch == '\n')
        {
            ch = fgetc(fp);
            while (ch == '\n')
            {
                e++;
                ch = fgetc(fp);
            }
        }
        else if (ch == '/')
        {
            ch = fgetc(fp);
            if (ch == '/')
            while (ch != '\n')
            {
                ch = fgetc(fp);
            }
            n++;
            ch = fgetc(fp);
        }
        else
        {
            c++;
            while (ch != '{'&&ch != '}'&&ch != '\n'&&ch != '/'&&ch != EOF)
            {
                ch = fgetc(fp);
            }
        }

    }
    printf("代码行为：%d\n", c);
    printf("空行为：%d\n", e);
    printf("注释行为：%d\n\n", n);
    fclose(fp);
}