# 软件技术基础第三次作业

#### 项目简介
{**实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。

具体命令行界面要求如下：

命令模式： wc.exe [参数] [文件名]

例：wc.exe -c file.txt 统计字符数**


#### 使用说明

1.  输入程序名wc.exe
2.  输入统计参数“-c”、“-w”、“-s”、“-a”
3.  输入文件路径

#### 运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/004939_4b8155e8_9718017.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/005102_fbc971ef_9718017.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/005126_14fb10e3_9718017.png "屏幕截图.png")